package com.j2core.sts.mathexpression;

/**
 * Created by sts on 7/24/14.
 */
public class ExpressionUtils {

    public static final String VALIDATION_ERROR="OOOPS!!!";

    /**
     * This function checks, is brackets count left and right are equals and
     *
     * @param expression Expression to analyze
     * @return is brackets count left and right are equals
     */
    public static boolean isBracketsCountEquals(String expression) {

        int bracketBalance = 0;

        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == '(') {
                bracketBalance++;
            } else if (expression.charAt(i) == ')') {
                bracketBalance--;
            }
            if(bracketBalance<0) return false;

        }

        return (bracketBalance == 0);
    }


    /**
     * Function trims leading\ending brackets if possible
     *
     * @param expression Expression
     * @return Trimmed expression
     */
    public static String trimBrackets(String expression) {

        while (expression.indexOf("(")== 0 && expression.lastIndexOf(")")==expression.length()-1){
            String subexpression = expression.substring(1, expression.length()-1);
            if(ExpressionUtils.isBracketsCountEquals(subexpression)){
                return subexpression;

            } else return expression;


        }
        return expression;
    }

}
