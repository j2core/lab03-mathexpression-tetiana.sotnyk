package com.j2core.sts.mathexpression;
import junit.framework.Assert;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by sts on 7/28/14.
 */
public class ParserDataOfStringInArrayListsTest {

    @Test
    public void testParserExpression_positiveCase() throws Exception {
        ArrayList<Double> testArrayListNum = new ArrayList<Double>();
        ArrayList<Character> testArrayListOper = new ArrayList<Character>();
        String[] positiveCases = {
                "5+3-4/2*5",
                "-4*5+2-1+45",
                "5*3-4+5/2"

        };
        String[] testSum = {
                "-2.0",
                "26.0",
                "13.5"
        };

        for (int i =0; positiveCases.length>i; i++) {


             Assert.assertEquals(ParserDataOfStringInArrayLists.parserExpression(positiveCases[i], testArrayListNum, testArrayListOper), testSum[i] );

        }
    }

}
