package com.j2core.sts.mathexpression;

import java.util.ArrayList;

/**
 * Created by sts on 7/17/14.
 */

public class ParserExpressionToIdentifyBrackets {
    public ParserExpressionToIdentifyBrackets() {
    }

    /**
     * Gets the expression as a string, parses into separate substring for braces gets results of these substring,
     * inserts them into the original expression until it receives the final result of the expression
     * @param expression  Expression to analyze
     * @param arrayListNum  ArrayList for storing numbers
     * @param arrayListOper ArrayList for storing operation
     * @throws Exception
     * Prints the result of the expression
     */
    void selectionInBracketSubstring(String expression, ArrayList arrayListNum, ArrayList arrayListOper) throws Exception {

        int index1, index2;
        String tmpResult, tmpSubStr1, tmpSubStr2;
        ParserDataOfStringInArrayLists parser = new ParserDataOfStringInArrayLists();
        //  If there are two brackets in the expression
        if (expression.contains("(") && expression.contains(")")) {
            while (expression.contains("(")) {
                // Selection of the pair of brackets
                index1 = expression.lastIndexOf("(");
                index2 = expression.indexOf(")", index1);
                String tmpExpression = expression.substring(index1 + 1, index2);
                // Parsing function call expression
                tmpResult = parser.parserExpression(tmpExpression, arrayListNum, arrayListOper);
                // Paste the result in the original expression
                if (index1 == 0) {
                    tmpSubStr1 = "";
                } else tmpSubStr1 = expression.substring(0, index1);
                if (index2 == expression.length() - 1) {
                    tmpSubStr2 = "";
                } else tmpSubStr2 = expression.substring(index2 + 1, expression.length());
                expression = tmpSubStr1 + tmpResult + tmpSubStr2;

//
            }
        }
        // If there is no brackets

        System.out.println("result expression = " + ParserDataOfStringInArrayLists.parserExpression(expression, arrayListNum, arrayListOper));

        }
    }
