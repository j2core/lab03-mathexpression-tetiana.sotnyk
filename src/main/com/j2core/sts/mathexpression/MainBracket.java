package com.j2core.sts.mathexpression;

import java.util.ArrayList;

/**
 * Created by sts on 7/17/14.
 */
// Calculation of mathematical expressions
public class MainBracket {

    public static void main(String[] args) throws Exception {

        String expression = "((.2.0+4)/4)+3-8/(3-1)*(4/(4+6-5/(4+8)))";

        System.out.println(expression);

        ArrayList<Double> arrayListNum = new ArrayList<Double>();
        ArrayList<Character> arrayListOper = new ArrayList<Character>();

        try {

            if (!ExpressionUtils.isBracketsCountEquals(expression)) {

                System.out.println(ExpressionUtils.VALIDATION_ERROR);
                throw new ArithmeticException();
            } else {
                expression = ExpressionUtils.trimBrackets(expression);
                ParserExpressionToIdentifyBrackets parserBracket = new ParserExpressionToIdentifyBrackets();
                parserBracket.selectionInBracketSubstring(expression, arrayListNum, arrayListOper);
            }
        }catch (ArithmeticException ex){
            System.out.println("Expression is not correct");
        }catch (Exception ex){
            System.out.println("can't get valid number in '" + expression + "'");
        }
    }

}