package com.j2core.sts.mathexpression;

import java.util.ArrayList;

/**
 * Created by sts on 7/17/14.
 */
//  Expression analysis on the elements and entering them in the collection
public class ParserDataOfStringInArrayLists {


    /**
     *  Expression analysis on numbers and operations, putting them in ArrayLists
     * @param str   Expression to analyze
     * @param arrayListNum  ArrayList for storing numbers
     * @param arrayListOper ArrayList for storing operation
     * @return Execution of actions in a different class(CalculationsDataFromArrayListNum) of his method, we return from it the String to answer
     * @throws Exception
     */
    public static String parserExpression(String str, ArrayList arrayListNum, ArrayList arrayListOper) throws Exception {


        int i = 0;
        int point = 0;
        boolean negative = false;
        //  Check for negative first number
        if( str.charAt(0) == '-' ){
            negative = true;
            str = str.substring( 1 );
        }

        // Finding and distiguish of the string numbers and operation, and entering them in the collection
        while (i<str.length()) {
            int j = i;
            while (i < str.length() && (Character.isDigit(str.charAt(i)) || str.charAt(i) == '.')) {

                if (str.charAt(i) == '.' && ++point > 1) {
                    throw new Exception("not valid number '" + str.substring(0, i + 1) + "'");
                }
                i++;
            }

            if (i == 0) throw new Exception("can't get valid number in '" + str + "'");


            double  value = Double.parseDouble(str.substring(j, i));

            if (negative) {
                value = -value;
                negative = false;
            }

            arrayListNum.add(value);
            point = 0;


            if (i<str.length()) {
                char operation = str.charAt(i);
                arrayListOper.add(operation);
                i++;
            }
            if(i<str.length()) {
                if ('-' == str.charAt(i)) {
                    negative = true;
                    i++;
                }
            }

        }
      // Data transmission on the calculation
        CalculationsDataFromArrayListNum calculations = new CalculationsDataFromArrayListNum();
        return calculations.calculationArrayList(arrayListNum, arrayListOper);



    }
}
