package com.j2core.sts.mathexpression;

import java.util.ArrayList;

/**
 * Created by sts on 7/17/14.
 */


public class CalculationsDataFromArrayListNum {

    public CalculationsDataFromArrayListNum() {
    }

    /**
     * Perform mathematical operations (from ArrayListOper) data from ArrayListNum
     *
     * @param arrayListNum  ArrayList for storing numbers
     * @param arrayListOper ArrayList for storing operation
     * @return Calculation result as a string
     */
    static String calculationArrayList(ArrayList arrayListNum, ArrayList arrayListOper) {

        String result;
        int index_availability;
        Double data1, data2, sum;
        // Performance all operations of '/'

            do {

                index_availability = arrayListOper.lastIndexOf('/');
                if (index_availability > -1) {
                    data1 = (Double) arrayListNum.get(index_availability);
                    data2 = (Double) arrayListNum.get(index_availability + 1);
                    //  Check division by zero

                    if (data2 == 0) {
                        System.out.println(ExpressionUtils.VALIDATION_ERROR);
                        throw new ArithmeticException();
                    } else {
                        arrayListNum.remove(index_availability + 1);
                        sum = data1 / data2;
                        arrayListNum.set(index_availability, sum);
                        arrayListOper.remove(index_availability);
                    }
                }

            } while (index_availability > -1);
            // Performance all operations of '*'
            do {
                index_availability = arrayListOper.lastIndexOf('*');
                if (index_availability > -1) {
                    data1 = (Double) arrayListNum.get(index_availability);
                    data2 = (Double) arrayListNum.get(index_availability + 1);
                    arrayListNum.remove(index_availability + 1);
                    sum = data1 * data2;
                    arrayListNum.set(index_availability, sum);
                    arrayListOper.remove(index_availability);

                }

            } while (index_availability > -1);
            // Performance all operations of '+' and '-'
            while (arrayListOper.size() > 0) {
                data1 = (Double) arrayListNum.get(0);
                data2 = (Double) arrayListNum.get(1);
                arrayListNum.remove(1);
                if (arrayListOper.get(0).equals('+')) {
                    sum = data1 + data2;

                } else {
                    sum = data1 - data2;

                }
                arrayListNum.set(0, sum);
                arrayListOper.remove(0);

            }

         result = arrayListNum.get(0).toString();
        arrayListNum.remove(0);
        // Result as a string
        return result;

    }
}
