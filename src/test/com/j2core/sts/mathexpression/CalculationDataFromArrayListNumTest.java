package com.j2core.sts.mathexpression;


import junit.framework.Assert;
import org.junit.Assume;
import org.junit.Test;

import java.util.ArrayList;

/**
* Created by sts on 7/25/14.
*/
public class CalculationDataFromArrayListNumTest {


    @Test
    public void testCalculationArrayList_positiveCases() {


        ArrayList<Double> testArrayListNumPosit = new ArrayList<Double>();
        ArrayList<Character>  testArrayListOperPosit = new ArrayList<Character>();

        String[][] stringNumPosit = new String[][] {
                {"3","2","1"},
                {"4","2","2"},
                {"1","5","5","2","3"}
        };
        String[] stringOperPosit =  new String[]{
                 "+-",
                "/*",
                "*+/-"
        };
        String[] arraySum = {"4.0","4.0","4.5"};

        for(int index = 0; index<arraySum.length; index++) {

            testArrayListNumPosit = ObjectDevelopment.developmentTestingArrayListNum(testArrayListNumPosit, stringNumPosit[index]);
            testArrayListOperPosit = ObjectDevelopment.developmentTestingArrayListOper(testArrayListOperPosit, stringOperPosit[index]);
            String sum = arraySum[index];

            Assert.assertEquals(CalculationsDataFromArrayListNum.calculationArrayList(testArrayListNumPosit, testArrayListOperPosit), sum);

        }
    }

    @Test  (expected = ArithmeticException.class)
    public void testCalculationArrayList_negativeCases() {
        ArrayList<Double> testArrayListNumNeg = new ArrayList<Double>();
        ArrayList<Character>  testArrayListOperNeg = new ArrayList<Character>();


        String[][] stringNumNeg = new String[][]{

                {"4","0","2"},
                {"1","5","5","0","3" }  ,

        };
        String[] stringOperNeg =  new String[]{

                "/*",
                "*+/-"
        };

        for(int index = 0; index<stringOperNeg.length; index++) {

            testArrayListNumNeg = ObjectDevelopment.developmentTestingArrayListNum(testArrayListNumNeg, stringNumNeg[index]);
            testArrayListOperNeg = ObjectDevelopment.developmentTestingArrayListOper(testArrayListOperNeg, stringOperNeg[index]);

            CalculationsDataFromArrayListNum.calculationArrayList(testArrayListNumNeg, testArrayListOperNeg);

        }

    }


}






