package com.j2core.sts.mathexpression;

import org.junit.Test;
import junit.framework.Assert;

import java.util.ArrayList;

/**
* Created by sts on 7/28/14.
*/
public class ParserExpressionToIdentifyBrecketsTest {

    @Test
    public static void testSelectionInBracketSubstring_positiveCase() throws Exception {

        ArrayList<Double> testArrayListNum = new ArrayList<Double>();
        ArrayList<Character> testArrayListOper = new ArrayList<Character>();
        String[] positiveCases = {
                "5+3-(4/2*5)",
                "(-4)*5+2-1+45",
                "(5*3-4+5/2)"

        };

        for(String positiveCase : positiveCases){
        Assert.assertTrue(Boolean.parseBoolean(ParserDataOfStringInArrayLists.parserExpression(positiveCase, testArrayListNum, testArrayListOper)));
    }
    }

    @Test
    public static void testSelectionInBracketSubstring_negativeCase() throws Exception {

        ArrayList<Double> testArrayListNum = new ArrayList<Double>();
        ArrayList<Character> testArrayListOper = new ArrayList<Character>();
        String[] negativeCases = {
                "5+3-4/2*5)",
                "(-4)*)5(+2-1+45",
                "(5*3-4+5/0)"

        };

        for(String negativeCase : negativeCases){
            Assert.assertFalse(Boolean.parseBoolean(ParserDataOfStringInArrayLists.parserExpression(negativeCase, testArrayListNum, testArrayListOper)));
        }
    }
}
