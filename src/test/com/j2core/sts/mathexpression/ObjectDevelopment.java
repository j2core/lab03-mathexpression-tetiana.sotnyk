package com.j2core.sts.mathexpression;

import java.util.ArrayList;

/**
 * Created by sts on 7/26/14.
 */
public class ObjectDevelopment {



    public static ArrayList<Double>  developmentTestingArrayListNum (ArrayList<Double> testArrayListNum, String[] stringNum) {

        double[] num = new double[stringNum.length];
        for(int i = 0; i<stringNum.length; i++){
            num[i] = Double.valueOf(stringNum[i]);
        }

        for (double aNum : num ) {
            testArrayListNum.add(aNum);
        }

        return testArrayListNum;
    }

    public static ArrayList<Character> developmentTestingArrayListOper (ArrayList<Character> testArrayListOper, String stringOper){

        char[] oper = stringOper.toCharArray();

        for (char anOper : oper) {
            testArrayListOper.add(anOper);
        }



        return testArrayListOper;
    }

}
