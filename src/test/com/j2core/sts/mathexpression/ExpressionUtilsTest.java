package com.j2core.sts.mathexpression;

import junit.framework.Assert;
import org.junit.Test;

/**
 * Created by sts on 7/24/14.
 */
public class ExpressionUtilsTest {


    @Test
    public void testIsBracketsCountEquals_positiveCases() {
        String[] positiveCases = new String[] {
                "(a+b)",
                "(a+b)+(c+d)"
        };

        for (String positiveCase : positiveCases) {
            Assert.assertTrue(ExpressionUtils.isBracketsCountEquals(positiveCase));
        }
    }

    @Test
    public void testIsBracketsCountEquals_negativeCases() {
        String[] negativeCases = new String[] {
                "(a+b))",
                "(a+b)+((c+d)",
                ")("
        };

        for (String negativeCase : negativeCases) {
            Assert.assertFalse(ExpressionUtils.isBracketsCountEquals(negativeCase));
        }
    }
     @Test
     public void testTrimBrackets_positiveCases(){

         String[] positiveCases = new String[]{
           "((2-3)+(5-7))",
           "((3+3)-(5))",
           "(4)"
         };
         for (String positiveCase : positiveCases) {
            Assert.assertEquals(ExpressionUtils.trimBrackets(positiveCase), positiveCase.substring(1,positiveCase.length()-1));
         }
     }
     @Test
     public void testTrimBrackets_negativeCases() {
         String[] negativeCases = new String[]{
                 "(2-3)+(5-7)",
                 "(3+3)-(5)",
                 "(4)+(2)"
         };
         String actual;
         for (String negativeCase : negativeCases) {
             actual = negativeCase;
             Assert.assertEquals(ExpressionUtils.trimBrackets(negativeCase), actual);
         }
     }


}
